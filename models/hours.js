const Joi = require('joi');
const mongoose = require('mongoose');

const workDaySchema = new mongoose.Schema({
  userid: {
    type: mongoose.ObjectId,
    required: true
  },
  minutes: {
    type: Number,
    required: true
  },
  startTime: {
    type: String,
    required: true
  },
  endTime: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  employer: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  }
});

const WorkDay = mongoose.model('Workday', workDaySchema);

function validateDay(workDay) {
  const timeSchema = {
    hours: Joi.number()
      .required()
      .min(0)
      .max(24),
    minutes: Joi.number()
      .required()
      .min(0)
      .max(60)
  };

  const dateSchema = {
    day: Joi.number()
      .required()
      .min(1)
      .max(31),
    month: Joi.number()
      .required()
      .min(1)
      .max(12),
    year: Joi.number()
      .required()
      .min(2018)
      .max(2025)
  };

  const schema = {
    startTime: timeSchema,
    endTime: timeSchema,
    employer: Joi.string().required(),
    date: dateSchema,
    description: Joi.string()
  };

  return Joi.validate(workDay, schema);
}

exports.WorkDay = WorkDay;
exports.validate = validateDay;
