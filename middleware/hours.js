const { validate } = require('../models/hours');
const _ = require('lodash');

module.exports.toMinutes = function(req, res, next) {
  const { error } = validate(req.body);
  if (error) return res.status(400).json({ error: error.details[0].message });

  const { startTime, endTime, date } = req.body;

  if (date.month < 10) {
    date.month = date.month.replace('0', '');
  }

  if (date.day < 10) {
    date.day = date.day.replace('0', '');
  }

  let start = new Date(
    date.year,
    date.month,
    date.day,
    startTime.hours,
    startTime.minutes
  );
  let end = new Date(
    date.year,
    date.month,
    date.day,
    endTime.hours,
    endTime.minutes
  );

  {
    let { minutes } = _.get(req.body, ['startTime']);
    if (minutes < 10) req.body.startTime.minutes = '0' + minutes;
  }
  {
    let { minutes } = _.get(req.body, ['endTime']);
    if (minutes < 10) req.body.endTime.minutes = '0' + minutes;
  }

  let differenceInMinutes = end - start;
  req.minutes = Math.round(differenceInMinutes / 1000 / 60);
  next();
};
