const express = require('express');

const hours = require('../routes/hours');
const users = require('../routes/users');
const rapports = require('../routes/rapports');

const error = require('../middleware/error');

module.exports = function(app) {
  app.use(express.json());
  app.use('/api/hours', hours);
  app.use('/api/user', users);
  app.use('/api/rapports', rapports);
  app.use(error);
};
