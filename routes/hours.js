const express = require('express');
const router = express.Router();
const _ = require('lodash');
const { WorkDay, validate } = require('../models/hours');
const { toMinutes } = require('../middleware/hours');
const auth = require('../middleware/auth');

router.post('/', auth, toMinutes, async (req, res) => {
  let test = _.pick(req.body, ['description', 'employer']);
  test.userid = req.user._id;
  test.minutes = req.minutes;

  let { day, month, year } = _.get(req.body, ['date']);
  month++;

  const ddmmyyy = day + '/' + month + '/' + year;
  test.date = ddmmyyy;

  {
    let { hours, minutes } = _.get(req.body, ['startTime']);
    test.startTime = hours + ':' + minutes;
  }

  {
    let { hours, minutes } = _.get(req.body, ['endTime']);
    test.endTime = hours + ':' + minutes;
  }

  const workday = new WorkDay(test);
  await workday.save();

  res.status(201).json({});
});

module.exports = router;
